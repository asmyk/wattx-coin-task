# Challenge task

This app is coded for https://github.com/WATTx/code-challenges/blob/master/frontend-engineer-challenge-top-coins.md challenge. 

## About project

This project is bootstraped by create-react-app. I'm using React, Redux - for state managament, Recharts - charts, React Bootstrap 4 - styles. This task is about fetch data from coinmarketcap, then parse and show them in desiered form. I used table and chart to show data.

## Installation

To install app run:

```
npm install
```

To start local server:

```
npm start
```

## Code structure
In /src folder there are 3 folders:

* components - components which are used for build pages. I mixed here smart and dumb components for flat as possible structure
* screens - components used for routing. There are main components who compose smaller blocks from /components
* store - place for redux store, actions, and reducers. I decided to put all files in one folder beceause there not much work do. When project grow, it's good to move actions/reducers to separate folders.

In screens I used HOC technique for download data. Normally, it can be done much cleaner by recompose, but in that case my approach is enough imo.

At some places I used prop types - they should be used everywhere, but I decided for fast prototyping leave them on critical places.

In additional time I would like to: Add service caching and refresh in every 30s for example, Then I would split table into smaller components(for sorting, icons, extra styles for trending), Also important thing is support diffrent languages than $ - this require decouple some code and requires diffrent currency formatting. Unit tests are also should be written when I get more time. Important thing is also parse data from marketcap - I used simple mapping and sorting algorithm. This can be done in much optimal way - but it require more time to acomplish that.
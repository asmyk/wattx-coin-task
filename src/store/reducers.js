import { combineReducers } from 'redux';
import {
    REQUEST_TICKER,
    RECEIVE_TICKER
} from './actions';

function mapTickerQuotes(items) {
    return items.map((item) => {
        let quote = item.quotes.USD; // hardcoded quote - if we want to add change currency we can refactor only that code without touching components
        return { ...item, price: quote.price, volume: quote.volume_24h, marketcap: quote.market_cap, priceChange: quote.percent_change_24h }
    })
}

function sortBy(items, fieldName = 'rank') {
    return items.sort((a, b) => (a[fieldName] - b[fieldName]));
}

function getTickers(
    state = {
        isFetching: false,
        itemsLimit: 100,
        items: []
    },
    action
) {
    switch (action.type) {
        case REQUEST_TICKER:
            return Object.assign({}, state, {
                isFetching: true,
                itemsLimit: action.howManyTicks
            });
        case RECEIVE_TICKER:
            return Object.assign({}, state, {
                isFetching: false,
                items: sortBy(mapTickerQuotes(Object.values(action.ticker)), 'rank'),
                lastUpdated: action.receivedAt
            });
        default:
            return state;
    }
}

function tickers(state = {}, action) {
    switch (action.type) {
        case RECEIVE_TICKER:
        case REQUEST_TICKER:
            return Object.assign({}, state, getTickers(state, action));
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    tickers
});

export default rootReducer;

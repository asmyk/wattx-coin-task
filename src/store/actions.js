import fetch from 'cross-fetch'

export const REQUEST_TICKER = 'REQUEST_TICKER'
export const RECEIVE_TICKER = 'RECEIVE_TICKER'

function requestTicker(howManyTicks) {
    return {
        howManyTicks,
        type: REQUEST_TICKER
    }
}

function receiveTicker(json) {
    return {
        type: RECEIVE_TICKER,
        ticker: json.data,
        receivedAt: Date.now()
    }
}

function fetchTicker(howManyTicks = 100) {
    return dispatch => {
        dispatch(requestTicker(howManyTicks));
        return fetch(`https://api.coinmarketcap.com/v2/ticker/?sort=rank&limit=${howManyTicks}`)
            .then(response => response.json())
            .then(json => dispatch(receiveTicker(json)))
    }
}

function shouldFetchTicker(state) {
    // here can be placed some cache
    // for example check if received date is bigger than 30 sec - if yes get data again. if not - get data directly from store
    return true;
}

export function fetchTickerIfNeeded(howManyTicks) {
    return (dispatch, getState) => {
        const state = getState();
        console.log(state)
        if (shouldFetchTicker(getState())) {
            return dispatch(fetchTicker(howManyTicks || state.tickers.itemsLimit));
        }
    }
}
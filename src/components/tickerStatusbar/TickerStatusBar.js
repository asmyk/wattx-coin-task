import React from 'react';
import { connect } from 'react-redux'

import { fetchTickerIfNeeded } from '../../store/actions';
import { Button, ButtonGroup } from 'reactstrap';

class TickerStatusBar extends React.Component {
    constructor(props) {
        super(props);

        this.onItemsLimitClick = this.onRadioBtnClick.bind(this);
    }

    onRadioBtnClick(rSelected) {
        this.props.dispatch(fetchTickerIfNeeded(rSelected));
    }

    render() {
        const { itemsLimit, lastUpdated } = this.props;

        return <div>
            <ButtonGroup>
                <Button color="primary" onClick={() => this.onItemsLimitClick(10)} active={itemsLimit === 10}>10 Entries</Button>
                <Button color="primary" onClick={() => this.onItemsLimitClick(50)} active={itemsLimit === 50}>50 Entries</Button>
                <Button color="primary" onClick={() => this.onItemsLimitClick(100)} active={itemsLimit === 100}>100 Entries</Button>
            </ButtonGroup>
            <p>Last updated: {lastUpdated}</p>
        </div>
    }
}

function mapStateToProps(state) {
    const { tickers } = state
    const {
        isFetching = true,
        lastUpdated,
        items = [],
        itemsLimit
    } = tickers || {
        isFetching: true,
        items: [],
        itemsLimit: 100
    }

    return {
        items,
        isFetching,
        itemsLimit,
        lastUpdated: new Date(lastUpdated).toUTCString()
    }
}

export default connect(mapStateToProps)(TickerStatusBar);
import React from 'react';
import NumberFormat from 'react-number-format';

export default (props) => {
    const { payload, active } = props;
    if (active) {
        const name = payload[0].payload.name;
        const marketcap = payload[0].payload.x;
        const volume = payload[0].payload.y;
        const priceChange = payload[0].payload.z;

        return <div className="custom-tooltip" style={{ backgroundColor: '#f9f9f9' }}>
            <p className="label">Name: {`${name}`}</p>
            <p>Market Capitalizaiton: <NumberFormat value={marketcap} displayType={'text'} thousandSeparator={true} prefix={'$'} /></p>
            <p>Volume (24h): <NumberFormat value={volume} displayType={'text'} thousandSeparator={true} prefix={'$'} /></p>
            <p>Price Change (24h): <NumberFormat value={priceChange} displayType={'text'} thousandSeparator={true} suffix={'%'} /></p>

        </div>
    }
    return null;
}
import React from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Routes } from '../../routes';

import Navbar from '../navbar/Navbar';
import TickerStatusBar from '../tickerStatusbar/TickerStatusBar';
import { Container } from 'reactstrap';

import LiquidityScreen from '../../screens/Liquidity';
import HomeScreen from '../../screens/Home';

const AppRouter = () => (
  <Container>
    <Router>
      <div>
        <Navbar></Navbar>
        <TickerStatusBar></TickerStatusBar>
        <Switch>

          <Route exact path={Routes.HOME} component={HomeScreen} />
          <Route path={Routes.LIQUIDITY} component={LiquidityScreen} />
        </Switch>
      </div>
    </Router>
  </Container>
);

export { AppRouter };
import React from 'react';
import { Table } from 'reactstrap';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';


class CurrenciesTable extends React.Component {
    createTable() {
        const { data } = this.props;
        let rows = [];

        for (let i = 0; i < data.length; i++) {
            let item = data[i];
            let key = item.id;

            const rank = <td>{item.rank}</td>;
            const name = <td >{item.name}</td>;
            const price = <td ><NumberFormat value={item.price} displayType={'text'} thousandSeparator={true} prefix={'$'} /></td>;
            const priceChange = <td ><NumberFormat value={item.priceChange} displayType={'text'} thousandSeparator={true} suffix={'%'} /></td>;
            const marketcap = <td ><NumberFormat value={item.marketcap} displayType={'text'} thousandSeparator={true} prefix={'$'} /></td>;
            const volume = <td ><NumberFormat value={item.volume} displayType={'text'} thousandSeparator={true} prefix={'$'} /></td>;

            rows.push(<tr key={key}>{rank}{name}{price}{priceChange}{marketcap}{volume}</tr>);
        }

        return rows;
    }

    render() {
        return (<Table>
            <thead>
                <tr>
                    <th>Rank</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Price Change (24h)</th>
                    <th>Market Cap</th>
                    <th>Volume</th>
                </tr>
            </thead>
            <tbody>
                {this.createTable()}
            </tbody>
        </Table>)
    }
}

CurrenciesTable.propTypes = {
    data: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        rank: PropTypes.number,
        price: PropTypes.number,
        priceChange: PropTypes.number,
        marketcap: PropTypes.number,
        volume: PropTypes.number
    }))
}

export { CurrenciesTable };
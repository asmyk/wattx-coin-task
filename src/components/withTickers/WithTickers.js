import React from 'react';
import { fetchTickerIfNeeded } from '../../store/actions';

import { connect } from 'react-redux'

// this HOC fetch data from redux and put data into component
export function withTickers(WrappedComponent) {
    class WithTickers extends React.PureComponent {
        componentDidMount() {
            const { dispatch, } = this.props;
            dispatch(fetchTickerIfNeeded());
        }

        render() {
            const { items, isFetching } = this.props;

            // could be added extra component for loading
            // error handling can be improved by try/again component - left for faster prototyping. now it works only on F5 :)
            return (
                <div>{isFetching && items.length === 0 && <h2>Loading data...</h2>}
                    {!isFetching && items.length === 0 && <h2>Could not download tickers. Please refresh and try again.</h2>}
                    {items.length > 0 && <WrappedComponent {...this.props}></WrappedComponent>}
                </div>)
        }
    }
    // for fast prototyping hardcoded that function here.
    function mapStateToProps(state) {
        const { tickers } = state
        const {
            isFetching = true,
            lastUpdated,
            items = []
        } = tickers || {
            isFetching: true,
            items: []
        }

        return {
            items,
            isFetching,
            lastUpdated
        }
    }

    return connect(mapStateToProps)(WithTickers);
}
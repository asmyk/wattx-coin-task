import React from 'react';
import {
    Navbar,
    Nav,
    NavItem
} from 'reactstrap';
import { Link } from 'react-router-dom'
import { Routes } from '../../routes';

export default () => (
    <div>
        <Navbar color="light" light expand="md">
            <Link to={Routes.HOME}>Coin App</Link>
            <Nav className="ml-auto" navbar>
                <NavItem>
                    <Link to={Routes.LIQUIDITY}>Liquidity</Link>
                </NavItem>
            </Nav>
        </Navbar>
    </div>
)
import React from 'react';
import { CurrenciesTable } from '../components/currenciesTable/CurrenciesTable';
import { withTickers } from '../components/withTickers/WithTickers';

class HomeScreen extends React.Component {
    render() {
        const { items } = this.props;

        return <CurrenciesTable data={items}></CurrenciesTable>;
    }
}



export default withTickers(HomeScreen);
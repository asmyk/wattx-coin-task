import React from 'react';
import { ScatterChart, ResponsiveContainer, Scatter, XAxis, YAxis, ZAxis, CartesianGrid, Tooltip } from 'recharts';
import { withTickers } from '../components/withTickers/WithTickers';
import TooltipChart from '../components/tooltipChart/TooltipChart';


function mapTickersIntoData(items) {
    return items.map(item => ({ x: item.marketcap, y: item.volume, z: Math.abs(item.priceChange), name: item.name }));
}

class LiquidityScreen extends React.Component {

    render() {
        const { items } = this.props;
        return (
            <ResponsiveContainer minWidth="500px" minHeight="500px" width="100%" height="80%">
                <ScatterChart margin={{ top: 60, left: 60 }}>
                    <CartesianGrid />
                    <XAxis dataKey={'x'} type="number" name='Market Capitalization' unit='$' />
                    <YAxis dataKey={'y'} type="number" name='Volume (24h)' unit='$' />
                    <ZAxis dataKey={'z'} range={[0, 100]} />
                    <Scatter name='Price Change (24h)' data={mapTickersIntoData(items)} fill='#8884d8' />
                    <Tooltip content={<TooltipChart />} />
                </ScatterChart>
            </ResponsiveContainer>)
    }
}

export default withTickers(LiquidityScreen);